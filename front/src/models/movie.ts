export interface Movie {
    _id: string
    name: string
    description: string
    type: [string]
    createdAt: string
    updatedAt: string
}