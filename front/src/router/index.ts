import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import LoginView from '../views/LoginView.vue'
import AboutView from '../views/AboutView.vue'
import { useUserStore } from '@/stores/user'
import { storeToRefs } from 'pinia'
import axios from 'axios'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/about',
      name: 'about',
      component: AboutView
    },
    {
      path: '/login',
      name: 'login',
      component: LoginView
    }
  ]
})

router.beforeEach((to, from, next) => {
  const localToken = localStorage.getItem('token')

  if(localToken) {
    axios.defaults.headers.common['Authorization'] = `${localToken}`

  } else {
    delete axios.defaults.headers.common['Authorization']
  }

  const userStore = useUserStore()
  const { token } = storeToRefs(userStore)

  if(localToken) token.value = localToken

  if (to.matched.some(record => record.meta.requiresAuth)) {
    if(!token.value) {
      next({name: 'login'})
    } else {
      next()
    }
  } else {
    next()

  }
})

export default router
