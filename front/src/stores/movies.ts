import axios from "axios"
import { defineStore } from "pinia"
import { ref } from "vue"
import type { Movie } from '../models/movie'

export const useMovieStore = defineStore('movies', () => {
    const movies = ref<Movie[]>([])
    const isLoading = ref<boolean>(false)
    const error = ref<string | null>(null)


    const getMovies = async () => {
        isLoading.value = true
        error.value = null

        try {
            const response = await axios.get('http://localhost:8080/api/movies')
            movies.value = response.data
        } catch (err) {
            error.value = 'Error lors de la récupération des films'
            console.log(err)
        } finally {
            isLoading.value = false
        }
    }

    return {
        movies,
        isLoading,
        error,
        getMovies
    }
})