import { ref } from 'vue'
import { defineStore } from 'pinia'
import axios from 'axios'
import { useRouter } from 'vue-router';


interface User {
  _id: string
  createdAt: string
  email: string
  password: string
  role: 'user' | 'admin'
  updatedAt: string
  username: string
}

export const useUserStore = defineStore('user', () => {
  const user = ref<User>()
  const token = ref<string>('')

  const router = useRouter()

  async function login (values: any) {
    await axios.post('http://localhost:8080/api/auth/login', {
      username: values.username,
      password: values.password
    })
    .then((response) => {
        user.value = response.data.user
        token.value = `Bearer ${response.data.token}`
        if(token.value) {
          localStorage.setItem('user', JSON.stringify(user.value))
          localStorage.setItem('token', `${token.value}`)
        }
        router.push('/')
    })
    .catch((err) => {
        console.log(err)
    })
  }

  function logout () {
    token.value = ''
    localStorage.removeItem('user')
    localStorage.removeItem('token')
    router.push('/login')
  }

  return { user, token, login, logout }
})
