import {test, expect, describe} from 'vitest'
import {shallowMount} from "@vue/test-utils";
import App from "../src/App.vue";
import Navbar from "../src/components/Navbar.vue";
import {RouterView} from "vue-router";

const wrapper = shallowMount(App, {
    context: {
        children: [Navbar, RouterView]
    }
})

describe('App.vue', () => {
    test('should render correctly', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('Should get correct components', () => {
        expect(wrapper.getComponent(Navbar))
        expect(wrapper.getComponent(RouterView))
    })
})