import {shallowMount} from "@vue/test-utils";
import HomeView from "../../src/views/HomeView.vue";
import {vi, describe, test, expect} from "vitest";
import { createTestingPinia } from '@pinia/testing'

const wrapper = shallowMount(HomeView, {
    global: {
        plugins: [createTestingPinia({initialState: {
                movies: {
                    movies: [{
                        "_id": "666340d6f3a8338f2a4d8eef",
                        "name": "Movie 1",
                        "description": "test 1",
                        "type": [
                            "Horror"
                        ],
                        "createdAt": "2024-06-07T17:18:14.668Z",
                        "updatedAt": "2024-06-07T17:18:14.668Z",
                        "__v": 0
                    },
                        {
                            "_id": "666340e5f3a8338f2a4d8ef1",
                            "name": "Movie 2",
                            "description": "test 2",
                            "type": [
                                "Drama"
                            ],
                            "createdAt": "2024-06-07T17:18:29.458Z",
                            "updatedAt": "2024-06-07T17:18:29.458Z",
                            "__v": 0
                        },
                        {
                            "_id": "66634119eea1e7ea4d3876b4",
                            "name": "Movie 3",
                            "description": "test 3",
                            "type": [
                                "Crime"
                            ],
                            "createdAt": "2024-06-07T17:19:21.661Z",
                            "updatedAt": "2024-06-07T17:19:21.661Z",
                            "__v": 0
                        },
                        {
                            "_id": "66634123eea1e7ea4d3876b6",
                            "name": "Movie 4",
                            "description": "test 4",
                            "type": [
                                "Drame"
                            ],
                            "createdAt": "2024-06-07T17:19:31.185Z",
                            "updatedAt": "2024-06-07T17:33:38.519Z",
                            "__v": 0
                        },
                        {
                            "_id": "6663412ceea1e7ea4d3876b8",
                            "name": "Movie 5",
                            "description": "test 5 ",
                            "type": [
                                "Horror"
                            ],
                            "createdAt": "2024-06-07T17:19:40.218Z",
                            "updatedAt": "2024-06-07T17:19:40.218Z",
                            "__v": 0
                        }]
                },
                getMovies: vi.fn(),
                isLoading: false,
                error: false
            },
            stubActions: false,
            createSpy: vi.fn,
        })]
    }
})

describe('HomeView.vue', () => {
    test('should render correctly', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('should get list of movies', () => {
        expect(wrapper.vm.movies.length).toBe(5)
    })
})