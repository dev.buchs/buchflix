import mongoose from 'mongoose'

export const connectDB = async () => {
    try {
        mongoose.set('strictQuery', false)
        mongoose.connect(process.env.MONGO_URI)
    } catch (err) {
        console.log(err)
        process.exit()
    }
}