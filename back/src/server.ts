import express from 'express';
import { connectDB }  from './config/db';
import cors from 'cors';

const dotenv = require('dotenv').config()
const app = express();

app.use(cors({origin: 'http://localhost:5173'}));
app.use(express.json())

// CONNEXION A LA DB
connectDB()

app.use("/api", require('./routes/index.routes'));

app.listen('8080', () => {
    console.log("Serveur à l'écoute");
});