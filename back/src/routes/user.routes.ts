import express from 'express'
import { authenticate } from '../middlewares/auth.middleware'

const router = express.Router()

router.get('/profile', authenticate, (req, res) => {
    res.json({ message: `Welcome ${req["user"].username}` })
})

module.exports = router;