import express from 'express'
const router = express.Router()
import { getPersons, getPersonById, setPerson, editPerson, deletePerson, getPersonMovies } from '../controllers/person.controller'

router.get('/', getPersons)
router.get('/:id', getPersonById)
router.get('/:id/movies', getPersonMovies)
router.post('/', setPerson)
router.put('/:id', editPerson)
router.delete('/:id', deletePerson)

module.exports = router