import { authenticate } from '../middlewares/auth.middleware'
import express from "express";
const router = express.Router()

router.use('/auth', require('./auth.routes'))
router.use('/movies', authenticate, require('./movies.routes'))
router.use('/user', authenticate, require('./user.routes'))
router.use('/person', authenticate, require('./person.routes'))

module.exports = router