import express from 'express';
const router = express.Router()
import { getMovies, getMovieByID, setMovies, editMovie, deleteMovie } from '../controllers/movie.controller'

router.get('/', getMovies);
router.get('/:id', getMovieByID);
router.post('/', setMovies)
router.put('/:id', editMovie)
router.delete('/:id', deleteMovie)

module.exports = router

