import {model, Schema, Document} from "mongoose";

export interface IPerson extends Document {
    firstname: string
    lastname: string
    birthDate?: Date
    biography?: string
}

const personSchema = new Schema<IPerson>(
    {
        firstname: {
            type: String,
            required: true
        },
        lastname: {
            type: String,
            required: true
        },
        birthDate: String,
        biography: String
    },
    {
        timestamps: true
    }
)

export const PersonModel = model('person', personSchema)