import { Schema, model, Document } from "mongoose";

export interface IMovie extends Document{
    _id: Schema.Types.ObjectId
    name: String
    description: String
    type: Array<string>
    actors?: [Schema.Types.ObjectId]
    director?: [Schema.Types.ObjectId]
}
const movieSchema = new Schema<IMovie>(
    {
        name: {
            type: String,
            required: true
        },
        description: {
            type: String,
            required: true
        },
        type: [{
            type: String,
            required: true
        }],
        actors: [{type: Schema.Types.ObjectId, ref: 'person'}],
        director: [{type: Schema.Types.ObjectId, ref: 'person'}]
    },
    {
        timestamps: true
    }
)

export const MovieModel = model('movie', movieSchema)