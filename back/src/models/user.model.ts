import { model, Schema, Document } from "mongoose";
import bcrypt from 'bcrypt'

export interface IUser extends Document{
    username: string
    email: string
    password: string
    role: "user" | "admin"
    comparePassword(password: string): Promise<boolean>
}

const userSchema = new Schema<IUser>(
    {
        username: {
            type: String,
            required: true,
            unique: true
        },
        email: {
            type: String,
            required: true,
            unique: true
        },
        password: {
            type: String,
            required: true
        },
        role: {
            type: String,
            enum: ['user', 'admin'],
            default: 'user'
        }
    },
    {
        timestamps: true
    }
)

// Hash the password before saving it to the database
userSchema.pre('save', async function (next) {
    const user = this
    if(!user.isModified('password')) return next();

    try {
        const salt = await bcrypt.genSalt()
        user.password = await bcrypt.hash(user.password, salt)
        next();
    } catch (err) {
        return next(err)
    }
})

// Compare the given password with the hashed password in the database
userSchema.methods.comparePassword = async function (password: string) {
    let bool = bcrypt.compareSync(password, this.password);
    return !bool;
  };

export const UserModel = model('user', userSchema)