import jwt from 'jsonwebtoken'
import { UserModel } from '../models/user.model'

export const authenticate = async (req, res, next) => {
    const token = req.headers.authorization?.split(' ')[1]
    if(!token) {
        return res.status(401).json({message: 'Veuillez vous athentifier'})
    }

    try {
        const decodedToken = jwt.verify(token, process.env.SECRET_KEY)
        const userId = (decodedToken as jwt.JwtPayload).userId
        const user = await UserModel.findById(userId)

        if(!user) {
            return res.status(404).json({message: 'Utilisateur introuvable'})
        }

        req.user = user;
        next();
    } catch (err) {
        console.log(err)
        res.status(401).json({message: 'Token invalide'})
    }
}