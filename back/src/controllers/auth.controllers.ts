import jwt  from 'jsonwebtoken'
import bcrypt from 'bcrypt'
import { UserModel } from '../models/user.model'

// Register new user
export const register = async (req, res, next) => {
    const { username, email, password } = req.body

    try {
        const hashedPassword = await bcrypt.hash(password, 10)
        const user = new UserModel({username, email, password: hashedPassword})
        await user.save()
        res.json({message: 'Nouvel utilisateur enregistré', user})
    } catch (err) {
        next(err)
    }
}

// Login with an existing user
export const login = async (req, res, next) => {
    const { username, password } = req.body

    try {
        const user = await UserModel.findOne({username})
        if(!user) {
            return res.status(404).json({message: 'Utilisateur introuvable'})
        }

        const passwordMatch = await user.comparePassword(password);
        if(!passwordMatch) {
            return res.status(401).json({message: 'Mot de passe incorrect'})
        }

        const token = jwt.sign({userId: user._id}, process.env.SECRET_KEY, {
            expiresIn: '365d'
        })
        res.status(200).json({token, user})
    } catch (err) {
        next(err)
    }
}

