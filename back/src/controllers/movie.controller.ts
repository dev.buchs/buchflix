import { IMovie, MovieModel } from '../models/movie.model'
import { Request, Response } from 'express';


export const getMovies = async (req: Request, res: Response): Promise<void> => {
    try {
        const movies: IMovie[] = await MovieModel.aggregate([
            {
                $lookup: {
                    from: "actor",
                    localField: "actor",
                    foreignField: "_id",
                    as: "actorDetails"
                }
            },
            {
                $lookup: {
                    from: "director",
                    localField: "director",
                    foreignField: "_id",
                    as: "directorDetails"
                }
            }
        ])
        res.status(200).json(movies)
    } catch (err) {
        console.log(err);
        res.status(500).json({message: "Une erreur est survenue", error: err})
    }
}

export const getMovieByID = async (req: Request, res: Response): Promise<void> => {
    try {
        const movie: IMovie = await MovieModel.findById(req.params.id)
            .populate('actors')
            .populate('director')
        res.status(200).json(movie)
    } catch (err) {
        console.log(err);
        res.status(400).json({message: "Ce film n'existe pas", error: err})
    }
}

export const setMovies = async (req: Request, res: Response): Promise<void> => {
    if (!req.body.name) {
        res.status(400).json({message: "Merci d'ajouter un titre"})
        return;
    }

    try {
        const movie: IMovie = await MovieModel.create({
            name: req.body.name,
            description: req.body.description,
            type: req.body.type
        })
    
        res.status(200).json(movie)
    } catch (err) {
        console.log(err);
        res.status(500).json({message: "Une erreur est survenue", error: err})
    }
}

export const editMovie = async (req: Request, res: Response): Promise<void> => {
    try {
        const { actors, name, description, type, director } = req.body

        const updateMovie: IMovie = await MovieModel.findByIdAndUpdate(
            req.params.id,
            {
                $addToSet: {
                    actors: {$each: actors},
                    director: {$each: director},
                    type: {$each: type}
                },
                $set: {name, description}
            },
            {new: true}
        )

        if(!updateMovie) {
            res.status(404).json({message: "Film non trouvé"})
            return
        }
    
        res.status(200).json(updateMovie)
    } catch (err) {
        console.log(err);
        res.status(500).json({message: "Une erreur est survenue", error: err})
    }
}

export const deleteMovie = async (req: Request, res: Response): Promise<void> => {
    try {
        const movie: IMovie = await MovieModel.findById(req.params.id)
        await movie.deleteOne();
        res.status(200).json({messsage: "Film supprimé avec succés", movie})
    } catch (err) {
        console.log(err);
        res.status(500).json({message: "Une erreur est survenue", error: err})
    }
}