import { Request, Response } from 'express';
import { PersonModel, IPerson } from '../models/person.model'
import {IMovie, MovieModel} from "../models/movie.model";

export const getPersons = async (req: Request, res: Response): Promise<void> => {
    try {
        const actors: IPerson[] = await PersonModel.find()
        res.status(200).json(actors)
    } catch (err) {
        console.log(err)
        res.status(500).json({message: "Une erreur est survenue", error: err})
    }
}

export const getPersonById = async (req: Request, res: Response): Promise<void> => {
    try {
        const actor: IPerson = await PersonModel.findById(req.params.id)
        const actorMovies: IMovie[] = await MovieModel.find({
            $or: [{actors: req.params.id}, {director: req.params.id}]
        })
        res.status(200).json({actor, actorMovies})
    } catch (err) {
        console.log(err)
        res.status(500).json({message: "Une erreur est survenue", error: err})
    }
}

export const setPerson = async (req: Request, res: Response): Promise<void> => {
    const { firstname, lastname, birthDate, biography } = req.body as Partial<IPerson>;

    const missingFields: Array<string> = [];
    if (!firstname) missingFields.push("un prénom");
    if (!lastname) missingFields.push("un nom");

    if(missingFields.length) {
        res.status(400).json({ message: `Merci d'ajouter ${missingFields.join(" et ")}` });
        return
    }

    if (birthDate && isNaN(new Date(birthDate).getTime())) {
        res.status(400).json({ message: "La date de naissance n'est pas valide" });
        return
    }

    try {
        const person: IPerson = await PersonModel.create({
            firstname,
            lastname,
            birthDate,
            biography
        })
        res.status(200).json(person)
    } catch (err) {
        console.log(err)
        res.status(500).json({message: "Une erreur est survenue", error: err})
    }
}

export const editPerson = async (req: Request, res: Response): Promise<void> => {
    try {
        const updatePerson: IPerson = await PersonModel.findByIdAndUpdate(
            req.params.id,
            req.body,
            {new: true}
        )

        if(!updatePerson) {
            res.status(404).json({message: "Personne non trouvée"})
            return
        }

        res.status(200).json(updatePerson)
    } catch (err) {
        console.log(err)
        res.status(500).json({message: "Une erreur est survenue", error: err})
    }
}

export const deletePerson = async(req: Request, res: Response): Promise<void> => {
    try {
        const person: IPerson = await PersonModel.findById(req.params.id)
        await person.deleteOne()
        res.status(200).json({message: "Personne supprimé avec succés", person})
    } catch (err) {
        console.log(err)
        res.status(500).json({message: "Une erreur est survenue", error: err})
    }
}

export const getPersonMovies = async (req: Request, res: Response): Promise<void> => {
    try {
        const movies: IMovie[] = await MovieModel.find({actors: req.params.id})
        res.status(200).json(movies)
    } catch (err) {
        console.log(err)
        res.status(500).json({message: "Une erreur est survenue", error: err})
    }
}